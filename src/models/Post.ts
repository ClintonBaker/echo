import mongoose from "mongoose";

const PostSchema = new mongoose.Schema({
  owner: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  originalPost: { type: mongoose.Schema.Types.ObjectId, ref: "Post" },
  echoedFrom: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  interactions: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "PostInteraction",
  },
  content: String,
  createdDate: { type: Date, default: Date.now },
});

export default mongoose.models.Post || mongoose.model("Post", PostSchema);
