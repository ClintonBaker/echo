import mongoose from "mongoose";

const PostInteractionSchema = new mongoose.Schema({
  post: { type: mongoose.Schema.Types.ObjectId, ref: "Post" },
  likes: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: "Comment" }],
});

export default mongoose.models.PostInteraction ||
  mongoose.model("PostInteraction", PostInteractionSchema);
