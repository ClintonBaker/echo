import mongoose from "mongoose";

const CommentSchema = new mongoose.Schema({
  parent: { type: mongoose.Schema.Types.ObjectId, ref: "Post" },
  owner: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  content: String,
  createdDate: { type: Date, default: Date.now },
});

export default mongoose.models.Comment ||
  mongoose.model("Comment", CommentSchema);
