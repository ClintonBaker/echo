import Head from "next/head";
import styled from "styled-components";
import NavBar from "./NavBar/NavBar";

const Layout = (props: any) => (
  <>
    <Head>
      <title>Echo</title>
      <meta
        name="description"
        content="A next generation social media web application"
      />
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon.png" />
    </Head>

    <Wrapper>
      <main>
        <NavBar />
        {props.children}
      </main>
    </Wrapper>
  </>
);

const Wrapper = styled.div`
   {
    background: #1c1c1c;
    min-height: 100vh;
  }
`;

export default Layout;
