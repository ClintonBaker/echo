import styled from "styled-components";
import Avatar from "react-nice-avatar";

import { PostT, CommentT } from "../../utilities/types";
import Image from "next/image";
import { useUser } from "../../utilities/hooks";
import { useState } from "react";

import Comment from "./Comment";
import { mutate } from "swr";

type PostProps = {
  post: PostT;
  refresh: () => void;
};

const Post = ({ post, refresh }: PostProps) => {
  const user = useUser();
  const [liked, setLiked] = useState<boolean>(
    !!post.interactions.likes.filter((likeId) => likeId === user._id).length
  );
  const [comments, setComments] = useState<CommentT[]>([
    ...post.interactions.comments,
  ]);
  const [displayComments, setDisplayComments] = useState(false);
  const [newComment, setNewComment] = useState("");

  const commentSubmitHandler = async () => {
    const reqParams = {
      method: "POST",
      body: JSON.stringify({
        parent: post._id,
        owner: user._id,
        content: newComment,
      }),
    };
    const res = await fetch(`api/post/${post._id}/comment`, reqParams);
    const data = await res.json();

    setComments([...comments, data.newComment]);
    setNewComment("");
  };

  const like = async () => {
    setLiked(!liked);
    await fetch(`api/post/${post._id}/like`, { method: "POST" });
    await refresh();
  };

  const echo = async () => {
    const url = `api/post/${post._id}/echo`;
    const reqParams = {
      method: "POST",
      body: JSON.stringify({
        echoedFrom: post.owner._id,
      }),
    };
    await fetch(url, reqParams);
    mutate("/api/timeline");
  };

  return (
    <Container>
      <AvatarContainer>
        <Avatar
          style={{ width: "60px", height: "60px" }}
          {...post.owner.avatarConfig}
          shape={"rounded"}
          bgColor={"transparent"}
        />
      </AvatarContainer>
      <DisplayPost>
        <Heading>
          <Name>{post.owner.userName}</Name>
          {post.echoedFrom && (
            <>
              <Image src="/echo.png" width="16px" height="16px" />
              <Name style={{ paddingLeft: "6px" }}>
                {post.echoedFrom.userName}
              </Name>
            </>
          )}
        </Heading>
        <Content>{post.content}</Content>
        <Footer>
          <Item>
            <Image
              src={`/heart-${liked ? "b" : "p"}.png`}
              width="20px"
              height="20px"
              onClick={like}
            />
          </Item>
          <Item
            onClick={() => {
              setDisplayComments(!displayComments);
            }}
          >
            <Image
              src={`/${displayComments ? "close-p" : "comment-e"}.png`}
              width="20px"
              height="20px"
            />
          </Item>
          <Item onClick={echo}>
            <Image src="/echo.png" width="20px" height="20px" />
          </Item>
        </Footer>
        {displayComments && (
          <CommentSection>
            <CommentBox>
              <Avatar
                style={{ width: "30px", height: "30px" }}
                {...post.owner.avatarConfig}
                shape={"rounded"}
                bgColor={"transparent"}
              />
              <CommentInput>
                <TextBox
                  placeholder="Add your thoughts"
                  value={newComment}
                  onChange={(e) => {
                    setNewComment(e.target.value);
                  }}
                  onKeyPress={(e) => {
                    e.key === "Enter" && commentSubmitHandler();
                  }}
                ></TextBox>
                <ReplyButton onClick={commentSubmitHandler}>Reply</ReplyButton>
              </CommentInput>
            </CommentBox>
            <DisplayComments>
              {comments.length ? (
                comments.map((comment) => (
                  <Comment key={comment._id} comment={comment} />
                ))
              ) : (
                <NoComments>No comments to show</NoComments>
              )}
            </DisplayComments>
          </CommentSection>
        )}
      </DisplayPost>
    </Container>
  );
};

const NoComments = styled.div`
   {
    padding: 10px;
    display: flex;
    justify-content: center;
  }
`;

const DisplayComments = styled.div`
   {
    padding-right: 10px;
    max-height: 300px;
    overflow-y: auto;
  }
`;

const ReplyButton = styled.button`
   {
    :hover {
      cursor: pointer;
    }
    background: none;
    border: none;
    margin-right: 10px;
    color: #999;
    font-weight: 600;
    font-size: 14px;
  }
`;

const CommentBox = styled.div`
   {
    display: flex;
    width: 98%;
    height: 38px;
  }
`;

const CommentInput = styled.div`
   {
    display: flex;
    width: 100%;
    border: 1px solid var(--accent);
    border-radius: 16px;
    height: 36px;
    margin-left: 6px;
  }
`;

const TextBox = styled.input`
   {
    :focus {
      outline: none;
    }
    ::placeholder {
      color: #ccc;
    }
    color: #ddd;
    background: none;
    border: none;
    margin-left: 10px;
    caret-color: #ddd;
    font-size: 16px;
    color: var(--accent);
    width: 100%;
  }
`;

const CommentSection = styled.div`
   {
    padding: 10px;
    border-top: 1px solid #333;
    display: flex;
    flex-flow: column;
  }
`;

const Item = styled.div`
   {
    :hover {
      cursor: pointer;
    }
  }
`;

const Container = styled.div`
   {
    color: var(--accent);
    border-radius: 5px;
    display: flex;
    flex-flow: row;
    background: #222;
    width: 100%;
    border-radius: 5px;
    margin-bottom: 18px;
    flex-wrap: wrap;
  }
`;

const AvatarContainer = styled.div`
   {
    position: relative;
    left: -90px;
    height: 64px;
    width: 64px;
    background: #222;
    border-radius: 5px;
    padding: 2px;
  }
`;

const Name = styled.div`
   {
    font-size: 12px;
    font-weight: 600;
    padding-right: 6px;
  }
`;

const Heading = styled.div`
   {
    padding: 12px;
    border-bottom: 1px solid #333;
    width: 100%;
    display: flex;
    align-items: center;
  }
`;

const DisplayPost = styled.div`
   {
    margin-left: -64px;
    width: 100%;
  }
`;

const Content = styled.div`
   {
    padding: 12px;
    color: var(--primary);
  }
`;

const Footer = styled.div`
   {
    border-top: 1px solid #333;
    padding: 12px 24px;
    display: flex;
    justify-content: space-between;
  }
`;

export default Post;
