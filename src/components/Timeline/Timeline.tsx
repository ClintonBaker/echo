import { useEffect, useState } from "react";
import styled from "styled-components";
import useSWR, { mutate } from "swr";
import { PostT } from "../../utilities/types";
import Post from "./Post";

const fetcher = (url: string) => {
  return fetch(url)
    .then((r) => r.json())
    .then((data) => {
      return { posts: data?.posts || null };
    });
};

const Timeline = () => {
  const { data, error } = useSWR("/api/timeline", fetcher);
  const [posts, setPosts] = useState<PostT[]>(data?.posts);

  useEffect(() => {
    setPosts(data?.posts);
  }, [data]);

  return (
    <Container>
      {posts?.map((post: PostT) => (
        <Post
          key={post._id}
          post={post}
          refresh={() => {
            mutate("/api/timeline");
          }}
        />
      ))}
    </Container>
  );
};

const Container = styled.div`
   {
    width: 100%;
  }
`;

export default Timeline;
