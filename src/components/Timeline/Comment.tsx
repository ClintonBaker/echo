import Avatar from "react-nice-avatar";
import styled from "styled-components";
import { CommentT } from "../../utilities/types";

export type CommentProps = { comment: CommentT };

const Comment = ({ comment }: CommentProps) => {
  return (
    <Container>
      <Avatar
        style={{ width: "30px", height: "30px" }}
        {...comment.owner.avatarConfig}
        shape={"rounded"}
        bgColor={"transparent"}
      />
      <ContentBox>
        <Header>{comment.owner.userName}</Header>
        <Content>{comment.content}</Content>
      </ContentBox>
    </Container>
  );
};

const Header = styled.div`
   {
    font-size: 12px;
  }
`;

const Content = styled.div`
   {
    margin-top: 8px;
  }
`;

const ContentBox = styled.div`
   {
    margin-left: 6px;
    border: 1px solid var(--accent);
    border-radius: 5px;
    padding: 10px;
  }
`;

const Container = styled.div`
   {
    display: flex;
    margin-top: 15px;
  }
`;

export default Comment;
