import React, { useState } from "react";
import Avatar from "react-nice-avatar";
import styled from "styled-components";
import { mutate } from "swr";
import { useUser } from "../../utilities/hooks";

type PostButtonProps = {
  submitPost: () => void;
};

type ClearButtonProps = {
  clearPost: () => void;
};

const PostButton = ({ submitPost }: PostButtonProps) => {
  return (
    <Button style={{ background: "var(--primary)" }} onClick={submitPost}>
      Post
    </Button>
  );
};

const ClearButton = ({ clearPost }: ClearButtonProps) => {
  return (
    <Button style={{ background: "#9da6af" }} onClick={clearPost}>
      Clear
    </Button>
  );
};

const PostCreationSection = () => {
  const [textValue, setTextValue] = useState<string>("");
  const user = useUser();

  const handleTextAreaChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setTextValue(e.currentTarget.value);
  };

  const submitPost = async () => {
    try {
      const reqParams = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ owner: user._id, content: textValue }),
      };
      const url = "/api/post";
      await fetch(url, reqParams);
      clearPost();
      mutate("/api/timeline");
    } catch (error: any) {
      console.error(error);
    }
  };

  const clearPost = () => {
    setTextValue("");
  };

  return (
    <Container>
      <AvatarContainer>
        <Avatar
          style={{ width: "60px", height: "60px" }}
          {...user.avatarConfig}
          shape={"rounded"}
          bgColor={"transparent"}
        />
      </AvatarContainer>
      <TextArea
        value={textValue}
        onChange={handleTextAreaChange}
        placeholder="What's on your mind?"
      />
      <TextAreaControl>
        <ClearButton clearPost={clearPost} />
        <PostButton submitPost={submitPost} />
      </TextAreaControl>
    </Container>
  );
};

const Container = styled.div`
   {
    display: flex;
    flex-flow: row;
    background: #222;
    width: 100%;
    border-radius: 5px;
    margin-bottom: 18px;
    padding: 15px;
    flex-wrap: wrap;
  }
`;

const AvatarContainer = styled.div`
   {
    position: relative;
    left: -104px;
    top: -15px;
    height: 64px;
    width: 64px;
    background: #222;
    border-radius: 5px;
    padding: 2px;
  }
`;

const TextArea = styled.textarea`
   {
    width: 100%;
    margin-left: -64px;
    padding: 12px;
    border-radius: 5px;
    resize: none;
    :focus {
      outline: none;
    }
  }
`;

const TextAreaControl = styled.div`
   {
    display: flex;
    justify-content: space-between;
    width: 100%;
    margin-top: 8px;
  }
`;

const Button = styled.button`
   {
    width: 65px;
    height: 30px;
    border-radius: 4px;
    border: none;
    font-weight: 600;
    color: #eee;
    :hover {
      cursor: pointer;
    }
  }
`;

export default PostCreationSection;
