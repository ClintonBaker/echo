import Image from "next/image";
import styled from "styled-components";
import { useUser } from "../../utilities/hooks";
import { Search } from "./Search";

const Logo = () => (
  <Item>
    <Icon>
      <Image src="/logo-icon-bl.png" width="34px" height="40px" />
    </Icon>
  </Item>
);

const SearchBox = () => (
  <Item style={{ width: "100%" }}>
    <Search placeholder="Search Echo" />
  </Item>
);

const HomeLink = () => (
  <Item>
    <Icon>
      <Image src="/house-b.png" width="26px" height="26px" />
    </Icon>
  </Item>
);

const Messages = () => (
  <Item>
    <Icon>
      <Image src="/chat-b.png" width="26px" height="34px" />
    </Icon>
  </Item>
);

const ProfileLink = () => (
  <Item>
    <Icon>
      <Image src="/profile-b.png" width="26px" height="26px" />
    </Icon>
  </Item>
);

const NavBar = () => {
  const user = useUser();

  return (
    <Wrapper>
      <Container>
        <LeftContainer>
          <Logo />
          <SearchBox />
        </LeftContainer>
        <RightContainer>
          <HomeLink />
          <Messages />
          <ProfileLink />
        </RightContainer>
      </Container>
    </Wrapper>
  );
};

const Item = styled.div`
   {
    padding-right: 14px;
  }
`;

const Wrapper = styled.div`
   {
    background: #2b2b2b;
    border-bottom: 1px solid #3b3b3b;
    position: sticky;
    top: 0;
    z-index: 1000;
  }
`;

const Container = styled.div`
   {
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 54px;
    width: 100%;
    padding-left: 120px;
    padding-right: 100px;
  }
`;

const LeftContainer = styled.div`
   {
    display: flex;
    flex-grow: 1;
    max-width: 450px;
    align-items: center;
  }
`;

const RightContainer = styled.div`
   {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

const Icon = styled.div`
   {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 48px;
    height: 48px;
    :hover {
      cursor: pointer;
      border-radius: 8px;
      background: var(--secondary);
    }
  }
`;

export default NavBar;
