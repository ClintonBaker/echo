import Image from "next/image";
import { useState } from "react";
import styled from "styled-components";

type SearchProps = {
  placeholder: string;
};

type SearchIconProps = {
  active: boolean;
};

const SearchIcon = ({ active }: SearchIconProps) => {
  return (
    <Icon>
      <Image
        src={active ? "/search-p.png" : "/search-b.png"}
        width="18px"
        height="18px"
      />
    </Icon>
  );
};

export const Search = (props: SearchProps) => {
  const [active, setActive] = useState<boolean>(false);
  const { placeholder } = props;

  return (
    <SearchBox style={{ background: active ? "#1c1c1c" : "var(--secondary)" }}>
      <SearchIcon active={active} />
      <SearchInput
        onFocus={() => {
          setActive(true);
        }}
        onBlur={() => {
          setActive(false);
        }}
        placeholder={placeholder}
      />
    </SearchBox>
  );
};

const SearchInput = styled.input`
   {
    background: transparent;
    border: none;
    padding: 5px;
    height: 38px;
    width: 100%;
    font-size: 16px;
    :focus {
      outline: none;
      caret-color: var(--accent);
      color: var(--accent);
      ::placeholder {
        color: #aaa;
      }
    }
    ::placeholder {
      color: var(--primary);
    }
  }
`;

const SearchBox = styled.div`
   {
    display: flex;
    align-items: center;
    border-radius: 4px;
    width: 100%;
  }
`;

const Icon = styled.div`
   {
    background: transparent;
    height: 38px;
    display: flex;
    align-items: center;
    margin-left: 10px;
  }
`;
