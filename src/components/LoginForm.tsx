import styled from "styled-components";

type FormProps = {
  isLogin: boolean;
  setIsLogin: (bool: boolean) => void;
  errorMessage: string;
  onSubmit: (e: any) => {};
};

const LoginForm = ({
  isLogin,
  setIsLogin,
  errorMessage,
  onSubmit,
}: FormProps) => {
  return (
    <Form onSubmit={onSubmit}>
      {!isLogin && (
        <Input type="text" name="email" placeholder="Email" required />
      )}

      <Input type="text" name="username" placeholder="Username" required />

      <Input type="password" name="password" placeholder="Password" required />

      {!isLogin && (
        <Input
          type="password"
          name="rpassword"
          placeholder="Verify Password"
          required
        />
      )}

      {errorMessage && <Error>*{errorMessage}</Error>}

      <Submit>
        <Button type="submit">{isLogin ? "Login" : "Signup"}</Button>
        <Link
          onClick={(e) => {
            e.preventDefault;
            setIsLogin(!isLogin);
          }}
        >
          {isLogin ? "I don't have an account" : "I already have an account"}
        </Link>
      </Submit>
    </Form>
  );
};

const Form = styled.form`
   {
    display: flex;
    flex-flow: column;
    align-items: center;
    width: 250px;
  }
`;

const Input = styled.input`
   {
    :focus {
      outline: none;
    }
    padding: 10px;
    margin-bottom: 7px;
    border: none;
    border-radius: 3px;
    width: 100%;
  }
`;

const Submit = styled.div`
   {
    display: flex;
    flex-flow: column;
    align-items: center;
    width: 100%;
    margin-top: 10px;
  }
`;

const Button = styled.button`
   {
    height: 35px;
    border: none;
    border-radius: 3px;
    width: 100%;
    background: var(--secondary);
    color: var(--primary);
    :hover {
      cursor: pointer;
    }
  }
`;

const Link = styled.a`
   {
    margin-top: 4px;
    color: var(--accent);
  }
`;

const Error = styled.p`
   {
    color: var(--error);
    margin-bottom: 4px;
    font-size: 12px;
  }
`;

export default LoginForm;
