import type { NextPage } from "next";
import styled from "styled-components";

import Layout from "../components/Layout";
import PostCreationSection from "../components/PostCreationSection/PostCreationSection";
import Timeline from "../components/Timeline/Timeline";
import { useUser } from "../utilities/hooks";

const Dashboard: NextPage = () => {
  const user = useUser({ redirectTo: "/login" });

  return (
    <Layout>
      {user ? (
        <Wrapper>
          <Container>
            <PostCreationSection />

            <Timeline />
          </Container>
        </Wrapper>
      ) : (
        <></>
      )}
    </Layout>
  );
};

const Wrapper = styled.div`
   {
    display: flex;
    justify-content: center;
  }
`;

const Container = styled.div`
   {
    display: flex;
    align-items: center;
    flex-flow: column;
    padding-top: 20px;
    width: 650px;
  }
`;

export default Dashboard;
