import { NextApiRequest, NextApiResponse } from "next";
import Post from "../../models/Post";
import { getLoginSession } from "../../utilities/auth";
import dbConnect from "../../utilities/mongoConnect";
require("../../models/PostInteraction");
require("../../models/Comment");

export default async function timeline(
  req: NextApiRequest,
  res: NextApiResponse
) {
  await dbConnect();
  try {
    const session = await getLoginSession(req);

    const userPosts = await Post.find({ owner: session?._doc?._id })
      .populate("owner")
      .populate({ path: "echoedFrom", select: ["userName"] })
      .populate({
        path: "interactions",
        populate: {
          path: "comments",
          populate: {
            path: "owner",
            select: ["_id", "avatarConfig", "userName"],
          },
        },
      })
      .sort({ createdDate: -1 });

    res.status(200).json({ posts: userPosts });
  } catch (error: any) {
    console.error(error);
    res.status(500).end("Authentication token is invalid, please log in");
  }
}
