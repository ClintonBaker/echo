import { NextApiRequest, NextApiResponse } from "next";
import { getLoginSession } from "../../utilities/auth";
import { findUser } from "../../utilities/user";

export default async function user(req: NextApiRequest, res: NextApiResponse) {
  try {
    const session = await getLoginSession(req);
    const user = (await findUser(session?._doc?.userName)) ?? null;

    res.status(200).json({ user });
  } catch (error: any) {
    console.error(error);
    res.status(500).end("Authentication token is invalid, please log in");
  }
}
