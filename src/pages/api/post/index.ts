import { NextApiRequest, NextApiResponse } from "next";
import Post from "../../../models/Post";
import PostInteraction from "../../../models/PostInteraction";
import User from "../../../models/User";
import dbConnect from "../../../utilities/mongoConnect";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const { body, method } = req;
  await dbConnect();

  switch (method) {
    case "POST":
      try {
        const postInteractions = await PostInteraction.create({});
        const postData = { ...body, interactions: postInteractions._id };
        const post = await Post.create(postData);
        postInteractions.post = post._id;
        postInteractions.save();
        User.updateOne({ _id: postData.owner }, { $push: { posts: post._id } });

        res.status(201).json({ success: true, data: post });
      } catch (error: any) {
        console.error(error);
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
  }
}
