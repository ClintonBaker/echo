import { NextApiRequest, NextApiResponse } from "next";
import PostInteraction from "../../../../models/PostInteraction";
import User from "../../../../models/User";
import { getLoginSession } from "../../../../utilities/auth";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const {
    method,
    query: { id },
  } = req;

  switch (method) {
    case "POST":
      try {
        const postId = id;
        const session = await getLoginSession(req);
        const userId = session?._doc?._id;
        const user = await User.findById(userId);
        const postInteractions = await PostInteraction.findOne({
          post: postId,
        });
        const userIndex = postInteractions.likes.indexOf(userId);
        const postIndex = user.likedPosts.indexOf(postId);

        if (userIndex < 0) {
          user.likedPosts.push(postId);
          postInteractions.likes.push(userId);
        } else {
          user.likedPosts.splice(postIndex, 1);
          postInteractions.likes.splice(userIndex, 1);
        }
        user.save();
        postInteractions.save();
        res.status(200).json({ success: true });
      } catch (error: any) {
        console.error(error);
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}
