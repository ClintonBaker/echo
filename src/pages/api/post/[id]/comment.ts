import { NextApiRequest, NextApiResponse } from "next";
import Comment from "../../../../models/Comment";
import PostInteraction from "../../../../models/PostInteraction";
import User from "../../../../models/User";
import { getLoginSession } from "../../../../utilities/auth";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const {
    body,
    method,
    query: { id },
  } = req;

  switch (method) {
    case "POST":
      try {
        const session = await getLoginSession(req);
        const userId = session?._doc?._id;
        const postId = id;
        const commentData = JSON.parse(body);
        const comment = await Comment.create(commentData);
        await comment.populate("owner");

        PostInteraction.updateOne(
          { post: postId },
          { $push: { comments: comment._id } }
        ).exec();
        User.updateOne(
          { _id: userId },
          { $push: { comments: comment._id } }
        ).exec();

        res.status(200).json({ success: true, newComment: comment });
      } catch (error: any) {
        console.error(error);
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}
