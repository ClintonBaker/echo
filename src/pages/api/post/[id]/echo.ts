import { NextApiRequest, NextApiResponse } from "next";
import Post from "../../../../models/Post";
import { getLoginSession } from "../../../../utilities/auth";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const {
    body,
    method,
    query: { id },
  } = req;

  switch (method) {
    case "POST":
      try {
        const { echoedFrom } = JSON.parse(body);
        const session = await getLoginSession(req);
        const userId = session?._doc?._id;
        const postId = id;
        const ogPost = await Post.findById(postId);
        const newPost = await Post.create({
          owner: userId,
          originalPost: ogPost._id,
          echoedFrom: echoedFrom,
          interactions: ogPost.interactions,
          content: ogPost.content,
        });

        res.status(200).json({ success: true, newPost: newPost });
      } catch (error: any) {
        console.error(error);
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}
