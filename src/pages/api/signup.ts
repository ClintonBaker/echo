import { NextApiRequest, NextApiResponse } from "next";
import nextConnect from "next-connect";
import passport from "passport";
import { localStrategy } from "../../utilities/password-local";
import { createUser } from "../../utilities/user";

passport.use(localStrategy);

export default nextConnect()
  .use(passport.initialize())
  .post(async function signup(req: NextApiRequest, res: NextApiResponse) {
    try {
      await createUser(req, res);
    } catch (error: any) {
      console.error(error);
      res.status(500).end(error.message);
    }
  });
