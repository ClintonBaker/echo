import passport from "passport";
import nextConnect from "next-connect";
import { localStrategy } from "../../utilities/password-local";
import { logIn } from "../../utilities/user";

passport.use(localStrategy);

export default nextConnect().use(passport.initialize()).post(logIn);
