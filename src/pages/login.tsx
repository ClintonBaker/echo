import { useState } from "react";
import Router from "next/router";
import styled from "styled-components";

import { useUser } from "../utilities/hooks";

import Image from "next/image";
import Layout from "../components/Layout";
import LoginForm from "../components/LoginForm";

type SubmitBody = {
  email?: string;
  username: string;
  password: string;
};

const Login = () => {
  const [isLogin, setIsLogin] = useState<boolean>(true);
  useUser({ redirectTo: "/", redirectIfFound: true });

  const [errorMsg, setErrorMsg] = useState("");

  async function handleSubmit(e) {
    e.preventDefault();

    if (errorMsg) setErrorMsg("");

    const body: SubmitBody = {
      username: e.currentTarget.username.value,
      password: e.currentTarget.password.value,
    };

    if (!isLogin) {
      body.email = e.currentTarget.email.value;
      if (body.password !== e.currentTarget.rpassword.value) {
        setErrorMsg(`The passwords don't match`);
        return;
      }
    }

    try {
      const res = await fetch(`/api/${isLogin ? "login" : "signup"}`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      });
      if (res.status === 200 || res.status === 201) {
        Router.push("/");
      } else {
        throw new Error(await res.text());
      }
    } catch (error: any) {
      console.error("An unexpected error happened occurred:", error);
      setErrorMsg(error.message);
    }
  }

  return (
    <Layout>
      <Container>
        <Header>
          <Image src="/logo-b.png" width="127px" height="50px" />
        </Header>
        <LoginForm
          isLogin={isLogin}
          setIsLogin={(bool: boolean) => {
            setIsLogin(bool);
            setErrorMsg("");
          }}
          errorMessage={errorMsg}
          onSubmit={handleSubmit}
        />
      </Container>
    </Layout>
  );
};

const Header = styled.div`
   {
    margin-bottom: 15px;
  }
`;

const Container = styled.div`
   {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 200px;
  }
`;

export default Login;
