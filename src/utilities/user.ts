import crypto from "crypto";
import { NextApiRequest, NextApiResponse } from "next";
import { genConfig } from "react-nice-avatar";

import dbConnect from "./mongoConnect";
import User from "../models/User";
import passport from "passport";
import { setLoginSession } from "./auth";

export const authenticate = (
  method,
  req: NextApiRequest,
  res: NextApiResponse
) =>
  new Promise((resolve, reject) => {
    passport.authenticate(method, { session: false }, (error, token) => {
      if (error) {
        reject(error);
      } else {
        resolve(token);
      }
    })(req, res);
  });

export const logIn = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const user = await authenticate("local", req, res);
    const session = { ...user };

    await setLoginSession(res, session);

    res.status(200).send({ done: true });
  } catch (error: any) {
    console.error(error);
    res.status(401).send(error.message);
  }
};

export async function createUser(req: NextApiRequest, res: NextApiResponse) {
  await dbConnect();

  try {
    const userData = req.body;
    const user = await User.find({ userName: userData.userName });

    if (!user.length) {
      const avatarConfig = genConfig({ bgColor: "transparent" });
      const salt = crypto.randomBytes(16).toString("hex");
      const hash = crypto
        .pbkdf2Sync(userData.password, salt, 1000, 64, "sha512")
        .toString("hex");
      const newUser = await User.create({
        userName: userData.username,
        email: userData.email,
        avatarConfig: avatarConfig,
        hash: hash,
        salt: salt,
      });

      const user = await authenticate("local", req, res);
      const session = { ...user };
      await setLoginSession(res, session);

      res.status(201).json({ success: true, data: newUser });
    } else {
      res
        .status(200)
        .json({ success: false, data: { error: "User already exists" } });
    }
  } catch (error) {
    console.error(error);
    res.status(400).json({ success: false });
  }
}

// Here you should lookup for the user in your DB
export async function findUser(username: string) {
  await dbConnect();
  const user = await User.find({ userName: username });
  return user[0];
}

// Compare the password of an already fetched user (using `findUser`) and compare the
// password for a potential match
export function validatePassword(user, inputPassword) {
  const inputHash = crypto
    .pbkdf2Sync(inputPassword, user.salt, 1000, 64, "sha512")
    .toString("hex");
  const passwordsMatch = user.hash === inputHash;
  return passwordsMatch;
}
