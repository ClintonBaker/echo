export type PostT = {
  _id: string;
  owner: UserT;
  originalPost?: string;
  echoedFrom?: UserT;
  interactions: {
    likes: string[];
    comments: CommentT[];
  };
  content: string;
};

export type UserT = {
  _id: string;
  userName: string;
  email: string;
  avatarConfig: Object;
  posts: PostT[];
  likedPosts: string[];
  following: UserT[];
};

export type CommentT = {
  _id: string;
  parent: string;
  owner: UserT;
  reference: string;
  likes: string[];
  content: string;
};
